variable "s3bucket" {
  type = string
  default = "vincenzo-np-bucket"
}

variable "dynamotable" {
  type = string
  default = "vincenzo-lock"
}