variable "s3bucket" {
    type = string
    default = ""
}

variable "dynamotable" {
    type = string
    default = ""
}

variable "awsregion" {
  type = string
  default = "eu-central-1"
}