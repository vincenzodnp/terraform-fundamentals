variable "http_port" {
  default = 443
}

variable "ssh_port" {
  default = 22
}